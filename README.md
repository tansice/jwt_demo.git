# SpringBoot集成Java-jwt例子

#### 项目介绍
Spring Boot集成Java-jwt
JWT简介
JSON Web Token（JWT）是一种开放的标准（RFC 7519），它定义了一种紧凑的和自包含的方式，用于在JSON对象之间安全传输信息。该信息可以被验证和信任，因为它是数字签名的。JWT可以使用密钥HMAC算法或RSA或ECDSA来签名。

JWT应用场景
授权：这是使用JWT最常见的方案。当用户登录后，每个后续请求将会在header带上JWT，允许用户访问允许使用该令牌的路由、服务和资源。单点登录是当今广泛使用JWT的一个特性，因为它具有较小的开销和易于跨不同域使用的能力。

信息交换：JWT是保证各方之间安全地传输信息的一种好方法。因为JWT可以被签名，例如使用公钥/私钥对，可以确保发件人是他们所说的人。另外，由于使用header和playload计算签名，还可以验证内容是否被篡改。

#### 软件架构
Spring Boot2.x
Java jwt 3.3.0

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
