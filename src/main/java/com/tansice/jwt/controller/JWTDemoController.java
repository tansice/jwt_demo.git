package com.tansice.jwt.controller;

import com.tansice.jwt.model.Result;
import com.tansice.jwt.service.JWTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/JWTDemo")
public class JWTDemoController {
    @Autowired
    JWTService jwtService;

    /**
     * 创建JWT
     *
     * @param data
     * @return
     */
    @RequestMapping("/createJwt")
    public Result createJwt(Map<String, String> data) {
        Result result = Result.ok();
        String token = jwtService.createJWT(data);
        result.put("token", token);
        return result;
    }

    /**
     * 验证JWT
     *
     * @param token
     * @return
     */
    @RequestMapping("/verifyJwt")
    public Result verifyJwt(String token) {
        Result result = Result.ok();
        try {
            jwtService.verfiyJWT(token);
        } catch (Exception ex) {
            result.setCode("-1");
            result.setMsg(ex.getMessage());
        }
        return result;
    }
}
