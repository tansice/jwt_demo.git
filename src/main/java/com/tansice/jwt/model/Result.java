package com.tansice.jwt.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回对象封装
 */
public class Result extends HashMap<String, Object> {
    private String code = "0";
    private String msg = "success";

    private final static String CODE = "code";
    private final static String MSG = "msg";

    public Result() {
        super();
        this.initResult();
    }

    public static Result ok() {
        Result result = new Result();
        return result;
    }

    public static Result ok(String msg) {
        Result result = new Result(msg);
        return result;
    }

    public static Result error(String msg) {
        Result result = new Result("-1", msg);

        return result;
    }

    public static Result error(String code, String msg) {
        Result result = new Result(code, msg);
        return result;
    }

    public static Result error() {
        Result result = new Result("-1", "system error");
        return result;
    }


    public Result(int initialCapacity, float loadFactor, String code, String msg) {
        super(initialCapacity, loadFactor);
        this.code = code;
        this.msg = msg;
        this.initResult();
    }

    public Result(int initialCapacity, String code, String msg) {
        super(initialCapacity);
        this.code = code;
        this.msg = msg;
        this.initResult();
    }

    public Result(String code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
        this.initResult();
    }

    public Result(String msg) {
        super();
        this.msg = msg;
        this.initResult();
    }

    public Result(Map<? extends String, ?> m, String code, String msg) {
        super(m);
        this.code = code;
        this.msg = msg;
        this.initResult();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
        put(CODE, code);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
        put(MSG, msg);
    }

    private void initResult() {
        put(CODE, code);
        put(MSG, msg);
    }
}
