package com.tansice.jwt.service;

import com.auth0.jwt.interfaces.Claim;

import java.util.Map;

/**
 * jwt服务
 * @author Trial
 * @since 1.0.0
 */
public interface JWTService {
    /**
     * 创建JWT
     * @param data
     * @return token
     */
    public String createJWT(Map<String,String> data);

    /**
     * 验证JWT
     * @param token
     * @return
     */
    public Map<String,Claim> verfiyJWT(String token);
}
