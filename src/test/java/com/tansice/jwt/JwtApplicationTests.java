package com.tansice.jwt;

import com.tansice.jwt.model.Result;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JwtApplicationTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testCreateJwtAndVerfiy() {
        String id = "12";
        String username = "Trial";
        //获取token
        Map<String, String> data = new HashMap<>();
        data.put("id", id);
        data.put("username", username);
        Result r1 = restTemplate.postForObject("/JWTDemo/createJwt", data, Result.class);
        String token = (String) r1.get("token");
        System.out.println(r1.toString());
        //验证token
        MultiValueMap<String, Object> postParameters = new LinkedMultiValueMap<>();
        postParameters.add("token", token);
        Result result = restTemplate.postForObject("/JWTDemo/verifyJwt", postParameters, Result.class);
        System.out.println(result.toString());
        Assert.assertEquals("JWT校验出错", "0", result.getCode());
    }

}
